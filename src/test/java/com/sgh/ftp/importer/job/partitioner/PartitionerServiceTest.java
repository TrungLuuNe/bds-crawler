package com.sgh.ftp.importer.job.partitioner;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.batch.item.ExecutionContext;

import com.sgh.ftp.importer.job.partitioner.PartitionerService;

import junit.framework.Assert;

public class PartitionerServiceTest {

    @SuppressWarnings("deprecation")
    @Test
    public void  divideThreadResource_ListSizeLessThanThreadSize_LastElementHavingFromIdBiggerThanToId() {
	int threadNumber = 10;
	List<String> listId = new ArrayList<String>((Arrays.asList("id1", "id2", "id3")));
	Map<String, ExecutionContext> threadResource = PartitionerService.divideThreadResource(listId, threadNumber);
	Assert.assertTrue("Last element must have toId less than fromId > toId",(Integer)threadResource.get("partition"+threadNumber).get("fromId")  - (Integer)threadResource.get("partition"+10).get("toId")>0);
    }
    
    @SuppressWarnings("deprecation")
    @Test
    public void  divideThreadResource_ListSizeBiggerThanThreadSize_LastElementHavingToIdEqualListSizeMinusOne() {
	int threadNumber = 2;
	List<String> listId = new ArrayList<String>((Arrays.asList("id1", "id2", "id3")));
	Map<String, ExecutionContext> threadResource = PartitionerService.divideThreadResource(listId, threadNumber);
	Assert.assertTrue("Last element must have toId equal list size -1", (Integer)threadResource.get("partition"+threadNumber).get("toId")== listId.size()-1);
    }
    
    @SuppressWarnings("deprecation")
    @Test
    public void  divideThreadResource_EmptyList_ThreadResourceIsEmpty() {
	int threadNumber = 2;
	List<String> listId = new ArrayList<String>();
	Map<String, ExecutionContext> threadResource = PartitionerService.divideThreadResource(listId, threadNumber);
	Assert.assertTrue("Thread resource must be empty", threadResource.isEmpty());
    }

}
