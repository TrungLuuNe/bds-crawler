/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sgh.ftp.importer.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/*@Configuration

@PropertySource("classpath:application.properties")
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", basePackages = {
"com.sgh.ftp.importer.job" })
public class DBConfig {

    @Bean
    public JdbcTemplate jdbcTemplate() {
	return new JdbcTemplate(localDataSource());
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterTemplate() {
	return new NamedParameterJdbcTemplate(localDataSource());
    }

    @Primary
    @Bean(name = "localDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource localDataSource() {
	return DataSourceBuilder.create().build();
    }
    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
	    @Qualifier("localDataSource") DataSource dataSource) {
	return builder.dataSource(dataSource).persistenceUnit("localDataSource").packages("com.sgh.ftp.importer.job")
		.build();
    }


}*/
