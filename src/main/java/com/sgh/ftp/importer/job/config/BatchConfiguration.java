
package com.sgh.ftp.importer.job.config;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialException;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.sgh.ftp.importer.job.listener.JobCompletionNotificationListener;
import com.sgh.ftp.importer.job.step.StepBuilder;


/**
 * Created by cuong.duong on 6/8/2017.
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    @Autowired
    private JobRepository jobRepository;

    @Autowired
    public StepBuilder stepBuilder;
    @Bean
    public JobLauncher asyncJobLauncher() {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return jobLauncher;
    }

    @Bean
    public Job uploadFileJob(JobCompletionNotificationListener listener) throws JSchException, SftpException, SerialException, SQLException {
        return jobBuilderFactory.get("uploadFileJob").preventRestart()
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .start(stepBuilder.buildUploadFileParallelStep())
            .build();
    }
    
    @Bean
    public Job crawlDataJob(JobCompletionNotificationListener listener) throws JSchException, SftpException, SerialException, SQLException {
        return jobBuilderFactory.get("crawlDataJob").preventRestart()
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .start(stepBuilder.buildUploadFileParallelStep())
            .build();
    }

   
}
