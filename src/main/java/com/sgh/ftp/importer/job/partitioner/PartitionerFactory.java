package com.sgh.ftp.importer.job.partitioner;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
@Configuration
public class PartitionerFactory {
    @StepScope
    @Bean
    public Partitioner buildFtpUploadResourcePartitioner(JdbcTemplate mysqlTemplate) {
        return new FtpOrgResourcePartitioner(mysqlTemplate);
    }

}
