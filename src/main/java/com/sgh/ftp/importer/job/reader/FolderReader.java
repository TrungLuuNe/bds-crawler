package com.sgh.ftp.importer.job.reader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.sgh.ftp.importer.helper.MimeTypeFactory;
import com.sgh.ftp.importer.helper.UUIDHelper;
import com.sgh.ftp.importer.job.BpUpload;
import com.sgh.ftp.importer.job.FtpOrg;
import com.sgh.ftp.importer.job.BpUpload.BpUploadBuilder;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class FolderReader implements ItemReader<BpUpload> {
    private final Logger log = LoggerFactory.getLogger(FolderReader.class);
    private String cpPath;
    private String importFolder;
    private int nextBpUploadIndex;
    private List<FtpOrg> listFtpOrg;
    private byte[] collectionId;
    private List<BpUpload> listBpUpload;

    FolderReader(List<FtpOrg> listFtpOrg, String cpPath, String importFolder) throws JSchException, SftpException {
	if(listFtpOrg !=null)
	{
	    	this.listFtpOrg = listFtpOrg;
		this.collectionId = UUIDHelper.createHexId();
		this.cpPath = cpPath;
		this.importFolder = importFolder;
		getAllBpUpload();
	}
	else
	{
	    
	}
    }

    private void getAllBpUpload() throws JSchException, SftpException {
	listBpUpload = new ArrayList<>();
	for (FtpOrg ftpOrg : listFtpOrg) {
	    listBpUpload.addAll(getBpUploadFromOrg(ftpOrg));
	}
    }

    private List<BpUpload> getBpUploadFromOrg(FtpOrg ftpOrg) throws JSchException, SftpException {
	List<BpUpload> listFileOfOrg = new ArrayList<>();
	JSch jsch = new JSch();
	Session session;
	session = jsch.getSession(ftpOrg.getUsername(), ftpOrg.getHost(), 22);
	session.setConfig("StrictHostKeyChecking", "no");
	session.setPassword(ftpOrg.getPassword());
	session.connect();
	Channel channel = session.openChannel("sftp");
	channel.connect();
	ChannelSftp sftpChannel = (ChannelSftp) channel;
	try {
	sftpChannel.cd(importFolder);
	}
	catch (SftpException e)
	{
	    log.error("Cannot access folder "+importFolder +" of ftp user "+ftpOrg.getUsername());
	    return listFileOfOrg;
	}
	@SuppressWarnings("unchecked")
	Vector<LsEntry> files = sftpChannel.ls(".");
	log.info("Found "+ (files.size()-2) +" files in dir of ftp user "+ ftpOrg.getUsername()  );

	for (LsEntry file : files) {
	    String imagePath = null;
	    if (file.getAttrs().isDir()) {
		continue;
	    }
	    String fileType= file.getFilename().substring(file.getFilename().lastIndexOf("."));
	    imagePath = UUID.randomUUID().toString()
		    + fileType;
	   if(MimeTypeFactory.createFileType(fileType) == null)
	       continue;
	    log.info("Reading file : " + file.getFilename() + " to " + imagePath );
	    sftpChannel.get(file.getFilename(), cpPath + imagePath);
	    BpUpload bpUpload = new BpUploadBuilder().setCollectionId(collectionId).setFileName(file.getFilename())
		    .setImagePath(imagePath).setMimeType(MimeTypeFactory.createFileType(fileType)).setOrganisationId(ftpOrg.getOrgBin())
		    .setFileSize(new File(cpPath + imagePath).length()).build();
	    log.info("Create upload document at with created date " +bpUpload.getCreatedAt() +" will be expired at" +bpUpload.getExpirationDate()); 
	    listFileOfOrg.add(bpUpload);
	    sftpChannel.rename(file.getFilename(), file.getFilename()+".imported");

	}

	sftpChannel.exit();
	session.disconnect();
	return listFileOfOrg;
    }

    @Override
    public BpUpload read() throws Exception {
	if(listFtpOrg ==null)
	    return null;
	else {
	BpUpload nextFile = null;

	if (nextBpUploadIndex < listBpUpload.size()) {
	    nextFile = listBpUpload.get(nextBpUploadIndex);
	    nextBpUploadIndex++;
	}

	return nextFile;
	}
    }

}
