
package com.sgh.ftp.importer.job;

import java.util.Date;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ftp-import")
public class MigrationResource {

    @Autowired
    @Qualifier("asyncJobLauncher")
    private JobLauncher jobLauncher;

    @Autowired
    private CrawlService crawlService;
    
    
   

 
  /*@Scheduled(cron = "${cron.expression}")
    private void launchJob() throws JobExecutionAlreadyRunningException, JobRestartException,
	    JobInstanceAlreadyCompleteException, JobParametersInvalidException {
	
	JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
		.toJobParameters();
	jobLauncher.run(job, jobParameters);

    }*/

   

    @GetMapping("")
    public ResponseEntity<String> dataMigrate(@RequestParam("city") String city,@RequestParam(value= "district", defaultValue = "") String district)
	    throws Exception {
    	String location = district.isEmpty()? city : district+"-"+city;
    	crawlService.crawlData(location, new Date());
	return new ResponseEntity<String>("Processing", HttpStatus.OK);
    }
   
    

}

