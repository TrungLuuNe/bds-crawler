package com.sgh.ftp.importer.job.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

import com.sgh.ftp.importer.job.partitioner.FtpOrgResourcePartitioner;

@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {
    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);
    private JobExecution _active;

    public void beforeJob(JobExecution jobExecution) {
        //create a lock
        synchronized(jobExecution) {
            if(_active!=null && _active.isRunning()) {
        	log.info("Stopped by current running job");
                jobExecution.stop();
            } else {
                _active=jobExecution;
            }
        }
    }

   
    @Override
    public void afterJob(JobExecution jobExecution) {
	
	log.info("!!! JOB FINISHED !!! EXECUTION STATUS: " + jobExecution.getStatus() + " "
		+ jobExecution.getAllFailureExceptions());
	synchronized(jobExecution) {
            if(jobExecution==_active) {
              _active=null; 
            }
        }

    }
}
