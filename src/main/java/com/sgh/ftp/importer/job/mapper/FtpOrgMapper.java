package com.sgh.ftp.importer.job.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.springframework.jdbc.core.RowMapper;

import com.sgh.ftp.importer.helper.UUIDHelper;
import com.sgh.ftp.importer.job.FtpOrg;

public class FtpOrgMapper implements RowMapper<FtpOrg> {

    @Override
    public FtpOrg mapRow(ResultSet rs, int rowNum) throws SQLException {
	FtpOrg ftpOrg = new FtpOrg();
	ftpOrg.setUsername(rs.getString("username"));
	ftpOrg.setPassword(rs.getString("password"));
	ftpOrg.setHost(rs.getString("host"));
	ftpOrg.setOrgBin(rs.getBytes("uuid"));
	return ftpOrg;
    }

}
