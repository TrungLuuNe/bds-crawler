package com.sgh.ftp.importer.job;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrawlService {
	@Autowired
	private PhoneDataRepository dataRepository;

	@SuppressWarnings("unchecked")
	public void crawlData(String location, Date toDate) throws IOException {

		String hostLocation = "http://www.batdongsan.vn/";
		if (location != null)
			location = location.replaceAll("\\s+", "-");
		else
			location = "";
		PhoneData phoneData = dataRepository.findTopByLocationOrderByCreatedAtDesc(location);
		Date lastDate = phoneData == null ? null : phoneData.getCreatedAt();
		String urlLocation = "http://www.batdongsan.vn/giao-dich/ban-nha-dat-tai-" + location;
		Document document = Jsoup.connect(urlLocation + ".html").userAgent("Mozilla").get();
		getData(document, urlLocation, location , hostLocation);
		urlLocation = "http://www.batdongsan.vn/giao-dich/cho-thue-nha-dat-tai-" + location;
		document = Jsoup.connect(urlLocation + ".html").userAgent("Mozilla").get();
		getData(document, urlLocation, location , hostLocation);
		

		}
	private void getData(Document document, String urlLocation, String location, String hostLocation) throws IOException
	{
		String pageUrl = "pageindex-";
		int page = 1;
		if (!document.getElementsByClass("selectpage").isEmpty())
			page = document.getElementsByClass("selectpage").get(0).children().size() - 1;
		System.out.println("total page: " + page);
		for (int i = 1; i <= page; i++) {
			System.out.println("current page: " + i);
			document = Jsoup.connect(urlLocation + "/" + pageUrl + i + ".html").get();
			Elements listData = document.getElementsByClass("Product_List ");
			listData = document.select(".Product_List > .wrap > .content1");
			listData.remove(0);

			for (Element data : listData) {
				String dataUrl = data.select("h2.P_Title > a[href]").attr("href");

				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				Date date = null;
					String dateText = data.select("div.al_author_tool.hidden-xs > :nth-child(2)").text();
					if (dateText == null || dateText.isEmpty())
						date = new Date();
					try{
					date = formatter.parse(data.select("div.al_author_tool.hidden-xs > :nth-child(2)").text());}
					catch (ParseException e) {
						date = new Date();
					}
					 {
						final Document dataDocument = Jsoup.connect(hostLocation + dataUrl).userAgent("Mozilla").get();
						String phoneNumber = dataDocument.select(".phone").get(0).text();
						PhoneData newData = new PhoneData();
						newData.setLocation(location);
						newData.setPhoneNumber(phoneNumber);
						newData.setCreatedAt(date);
						dataRepository.save(newData);
					}
				} 
			}

	}

}
