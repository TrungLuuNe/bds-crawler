package com.sgh.ftp.importer.job;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.sgh.ftp.importer.helper.UUIDHelper;

public class BpUpload {
    private static final String LOCALHOST_IP = "127.0.0.1";
    private byte[] id;
    private byte[] collectionId;
    private long fileSize;
    private byte[] organisationId;
    private String uploadIp;
    private String fileName;
    private String imagePath;
    private Date expirationDate;
    private Date createdAt;
    private Date updatedAt;
    private int isDone;
    private String mimeType;

    public byte[] getId() {
	return id;
    }

    public void setId(byte[] id) {
	this.id = id;
    }

    public byte[] getOrganisationId() {
	return organisationId;
    }

    public void setOrganisationId(byte[] organisationId) {
	this.organisationId = organisationId;
    }

    public byte[] getCollectionId() {
	return collectionId;
    }

    public void setCollectionId(byte[] collectionId) {
	this.collectionId = collectionId;
    }

    public long getFileSize() {
	return fileSize;
    }

    public void setFileSize(long fileSize) {
	this.fileSize = fileSize;
    }

    public String getUploadIp() {
	return uploadIp;
    }

    public void setUploadIp(String uploadIp) {
	this.uploadIp = uploadIp;
    }

    public String getFileName() {
	return fileName;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

    public String getImagePath() {
	return imagePath;
    }

    public void setImagePath(String imagePath) {
	this.imagePath = imagePath;
    }

    public Date getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
	this.expirationDate = expirationDate;
    }

    public Date getCreatedAt() {
	return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
	return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
	this.updatedAt = updatedAt;
    }

    public int getIsDone() {
	return isDone;
    }

    public void setIsDone(int isDone) {
	this.isDone = isDone;
    }

    public String getMimeType() {
	return mimeType;
    }

    public void setMimeType(String mimeType) {
	this.mimeType = mimeType;
    }

    private static BpUpload createNewBpUpload(byte[] collectionId, String fileName, String imagePath, String mimeType,
	    byte[] organisationId, long fileSize) {
	LocalDateTime now = LocalDateTime.now();
	Date exirationDate = Date.from(LocalDateTime.now().plusDays(7).atZone(ZoneId.systemDefault()).toInstant());
	Date currentDate = new Date();
	BpUpload bpUpload = new BpUpload();
	bpUpload.setId(UUIDHelper.createHexId());
	bpUpload.setIsDone(0);
	bpUpload.setExpirationDate(exirationDate);
	bpUpload.setCreatedAt(currentDate);
	bpUpload.setUpdatedAt(currentDate);
	bpUpload.setUploadIp(LOCALHOST_IP);
	bpUpload.setCollectionId(collectionId);
	bpUpload.setFileName(fileName);
	bpUpload.setImagePath(imagePath);
	bpUpload.setMimeType(mimeType);
	bpUpload.setOrganisationId(organisationId);
	bpUpload.setFileSize(fileSize);
	return bpUpload;
    }

    public static class BpUploadBuilder

    {
	private byte[] nestedCollectionId;
	private String nestedFileName;
	private String nestedImagePath;
	private String nestedMimeType;
	private byte[] nestedOrganisationId;
	private long nestedFileSize;
	
	public BpUploadBuilder setFileSize(long fileSize) {
	    this.nestedFileSize = fileSize;
	    return this;
	}
	
	public BpUploadBuilder setCollectionId(byte[] collectionId) {
	    this.nestedCollectionId = collectionId;
	    return this;
	}

	public BpUploadBuilder setFileName(String fileName) {
	    this.nestedFileName = fileName;
	    return this;
	}

	public BpUploadBuilder setImagePath(String imagePath) {
	    this.nestedImagePath = imagePath;
	    return this;
	}

	public BpUploadBuilder setMimeType(String mimeType) {
	    this.nestedMimeType = mimeType;
	    return this;

	}

	public BpUploadBuilder setOrganisationId(byte[] organisationId) {
	    this.nestedOrganisationId = organisationId;
	    return this;
	}

	public BpUpload build() {
	    return createNewBpUpload(nestedCollectionId, nestedFileName, nestedImagePath, nestedMimeType,
		    nestedOrganisationId,nestedFileSize);
	}

    }

}
