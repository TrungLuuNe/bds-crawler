package com.sgh.ftp.importer.job.partitioner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ExecutionContext;

public class PartitionerService {
   static Map<String, ExecutionContext> divideThreadResource(List<?> listId, int threadNumber) {
	Map<String, ExecutionContext> threadResource = new HashMap<String, ExecutionContext>();
	int range = listId.size() / threadNumber;
	int fromIndex = 0;
	int toIndex = range;
	if (listId.size() == 0) {
	    return threadResource;

	} else {

	    for (int i = 1; i <= threadNumber; i++) {
		ExecutionContext value = new ExecutionContext();
		value.putInt("fromId", fromIndex);
		if (i == threadNumber) {
		    value.putInt("toId", listId.size() - 1);
		} else {
		    value.putInt("toId", toIndex);
		}
		value.putString("name", "Thread" + i);
		threadResource.put("partition" + i, value);
		fromIndex = toIndex + 1;
		toIndex = fromIndex + range;
	    }
	    return threadResource;
	}
    }
}
