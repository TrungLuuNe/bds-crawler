package com.sgh.ftp.importer.job.reader;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import javax.sql.rowset.serial.SerialException;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.sgh.ftp.importer.job.BpUpload;
import com.sgh.ftp.importer.job.FtpOrg;
import com.sgh.ftp.importer.job.partitioner.FtpOrgResourcePartitioner;

@Configuration
public class ReaderFactory {
    /*protected static class PlayerFieldSetMapper implements FieldSetMapper<Player> {
	    public Map mapFieldSet(FieldSet fieldSet) {
		Map data = St
		Properties properties = new Properties();
		for (String key : properties.stringPropertyNames()) {
		    String value = properties.getProperty(key);
		    data.put(key, Integer.valueOf(value));
		}
	    }
	}
    @Autowired*/
    DataSource dataSource;
    @Value("${cp.folder}")
    private String cpFolder;
    @Value("${import.folder}")
    private String importFolder;
    @StepScope
    @Bean
    public ItemReader<BpUpload> buildFtpOrgReader(@Value("#{stepExecutionContext[fromId]}") int fromId,
	    @Value("#{stepExecutionContext[toId]}") int toId, @Value("#{stepExecutionContext[name]}") String name) throws JSchException, SftpException, SerialException, SQLException {
	List<FtpOrg> listFtpOrg = null;
	if (fromId < 0 || toId >= FtpOrgResourcePartitioner.listFtpOrg.size() || fromId > toId) {
	    listFtpOrg = null;
	} else {

	    listFtpOrg = FtpOrgResourcePartitioner.listFtpOrg.subList(fromId, toId + 1);
	}
	FolderReader folderReader = new FolderReader(listFtpOrg, System.getProperty("user.home")+"/"+ cpFolder+"/", importFolder);
	return folderReader; 
    }
    
    
   
}
