package com.sgh.ftp.importer.job.partitioner;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.sgh.ftp.importer.job.FtpOrg;
import com.sgh.ftp.importer.job.mapper.FtpOrgMapper;
import com.sgh.ftp.importer.sql.SqlUploadBuilder;

public class FtpOrgResourcePartitioner implements Partitioner {
    private static Logger log = Logger.getLogger(FtpOrgResourcePartitioner.class.getName());
    public static List<FtpOrg> listFtpOrg = null;
    private JdbcTemplate mysqlTemplate;


    public FtpOrgResourcePartitioner(JdbcTemplate mysqlTemplate) {
	this.mysqlTemplate = mysqlTemplate;
    }

    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {
	listFtpOrg = mysqlTemplate.query(SqlUploadBuilder.GET_ORG_WITH_FTP_QUERY, new FtpOrgMapper());
	log.info("Total number of ftp org : " + listFtpOrg.size());
	return PartitionerService.divideThreadResource(listFtpOrg, gridSize);
    }

}