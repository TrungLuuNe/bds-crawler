package com.sgh.ftp.importer.job;

import java.sql.Blob;

public class FtpOrg {
    private String username;
    private String password;
    private String host;
    private byte[] orgBin;

    public byte[] getOrgBin() {
	return orgBin;
    }

    public void setOrgBin(byte[] orgBin) {
	this.orgBin = orgBin;
    }

    public String getHost() {
	return host;
    }

    public void setHost(String host) {
	this.host = host;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

}
