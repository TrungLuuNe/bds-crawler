package com.sgh.ftp.importer.job.step;

import java.sql.SQLException;

import javax.sql.rowset.serial.SerialException;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.sgh.ftp.importer.job.BpUpload;
import com.sgh.ftp.importer.job.PhoneData;
import com.sgh.ftp.importer.job.partitioner.PartitionerFactory;
import com.sgh.ftp.importer.job.reader.ReaderFactory;
import com.sgh.ftp.importer.job.writer.WriterFactory;

@Configuration
public class StepBuilder {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ReaderFactory readerBuilder;

    @Autowired
    private PartitionerFactory paritionerBuilder;

    @Autowired
    private WriterFactory writerBuilder;

    @Autowired
    private JdbcTemplate mysqlTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final int THREAD_SIZE = 10;

    private final int DOCUMENT_UPDATE_CHUNK_SIZE = 100;
    private Step buildUploadfileSlaveStep() throws JSchException, SftpException, SerialException, SQLException {
	return stepBuilderFactory.get("uploadFileSlaveStep").<BpUpload, BpUpload>chunk(DOCUMENT_UPDATE_CHUNK_SIZE)
		.reader(readerBuilder.buildFtpOrgReader(0, 0, null))
		.writer(writerBuilder.buildBpUploadWriter(namedParameterJdbcTemplate)).build();
    }

    
    @Bean
    public Step buildUploadFileParallelStep() throws JSchException, SftpException, SerialException, SQLException {
	return stepBuilderFactory.get("updateUploadFileStep")
		.partitioner("slaveUploadFilePartitioner",paritionerBuilder.buildFtpUploadResourcePartitioner(mysqlTemplate))
		.gridSize(THREAD_SIZE).step(buildUploadfileSlaveStep()).taskExecutor(new SimpleAsyncTaskExecutor())
		.build();
    }

}
