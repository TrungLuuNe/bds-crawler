package com.sgh.ftp.importer.job.writer;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.sgh.ftp.importer.job.BpUpload;
import com.sgh.ftp.importer.sql.SqlUploadBuilder;

/**
 * Created by cuong.duong on 6/7/2017.
 */
@Configuration
public class WriterFactory {

    @Autowired
    public DataSource localDataSource;

    @Bean
    public ItemWriter<BpUpload> buildBpUploadWriter(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
	JdbcBatchItemWriter<BpUpload> bpUploadWriter = new JdbcBatchItemWriter<BpUpload>();
	bpUploadWriter.setDataSource(localDataSource);
	bpUploadWriter.setJdbcTemplate(namedParameterJdbcTemplate);
	bpUploadWriter.setSql(SqlUploadBuilder.INSERT_BP_UPLOAD_QUERY);
	ItemSqlParameterSourceProvider<BpUpload> paramProvider = new BeanPropertyItemSqlParameterSourceProvider<>();
	bpUploadWriter.setItemSqlParameterSourceProvider(paramProvider);
	return bpUploadWriter;
    }

}
