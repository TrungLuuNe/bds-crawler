package com.sgh.ftp.importer.job;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface PhoneDataRepository extends JpaRepository<PhoneData,Long> {
	public PhoneData findTopByLocationOrderByCreatedAtDesc( String location);
}
