package com.sgh.ftp.importer.helper;

import java.util.Random;

public class UUIDHelper {

    public static byte[] createHexId() {
	byte[] b = new byte[16];
	new Random().nextBytes(b);
	return b;
    }
}
