package com.sgh.ftp.importer.helper;

public class MimeTypeFactory {
 public static String createFileType(String input)
 {
     switch (input ) {
     case ".tif":
         return "image/tif";
     case ".pdf":
         return "application/pdf";
     case ".png":
         return "image/png";
     case ".jpeg":
         return"image/jpeg";
     case ".jpg":
         return"image/jpeg";
     default:
         return null;
 }
 }
}
