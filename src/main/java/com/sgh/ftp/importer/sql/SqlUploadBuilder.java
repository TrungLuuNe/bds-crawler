package com.sgh.ftp.importer.sql;

public class SqlUploadBuilder {
public static String GET_ORG_WITH_FTP_QUERY = "SELECT cp_organisations.id AS 'uuid' ,HEX(cp_organisations.id) AS 'orgId', username, password, url AS 'host'  FROM cp_organisations  JOIN cp_ftp_accesses ON (cp_organisations.id = cp_ftp_accesses.organisation_id )  where ftp_usage=1 and import=1";
public static String INSERT_BP_UPLOAD_QUERY= "INSERT INTO `sgh_bk`.`bp_uploads` (`id`,`collection_id`,`file_size`,`organisation_id`,`upload_ip`,`file_name`,`image_path`,`expiration_date`,`created_at`,`updated_at`, `is_done`, `mime_type`) VALUES (:id,:collectionId,:fileSize,:organisationId,:uploadIp,:fileName,:imagePath,:expirationDate,:createdAt,:updatedAt,:isDone,:mimeType)";
}
